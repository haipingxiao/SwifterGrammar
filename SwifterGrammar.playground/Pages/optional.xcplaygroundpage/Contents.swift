//: [Previous](@previous)

import Foundation

/*********************************可选项*****************************/
/*********************************************************************
 1.可选值:可以有值,可以为nil(用 ? 表示可选值)
 ********************************************************************/
//URL为可选项
let URL = NSURL(string:"http://www.baidu.com/")

//str为可选项
var str : String? = "SwifterGrammar";

//var的可选项默认值为nil
var a : Int?
print(a ?? "")

/*************************** if let *************************/
// if let : 确保 myUrl 有值，才会进入分支
if let myUrl = URL{
    print(myUrl)
}

var aName:String? = "Swifter Grammar"
var aAge:Int? = 18
if let name = aName,let age = aAge {
    print(name + String(age))
}

//可以对值进行修改
if var name = aName, let age = aAge{
    name = "list"
    print(name + String(age))
}

/****************************** guard let ***************************/
/*********************************************************************
 1.guard let 和 if let 相反，表示一定有值，没有就直接返回
 2.降低分支层次结构
 ********************************************************************/
class test{
    func demo() {
        let aNick: String? = "Swifter Grammar"
        let aAge: Int? = 10
        guard let nick = aNick,let age = aAge else{
            print("nil")
            return
        }
        print("guard let:" + nick + String(age))
    }
}

var t = test()
t.demo()

/*******************************强制解包***************************************/
var dataList:[String]?
dataList = ["zhangsan","list"]
/******************************************************************************
 1.dataList? 表示datalist可能为nil
 2.如果为nil，.count不会报错，仍然返回nil
 3.如果不为nil，.count执行，返回数组元素个数
 ******************************************************************************/

let count = dataList?.count ?? 0
print(String(count))

//表示dataList一定有值，否则会出错
let count2 = dataList!.count


