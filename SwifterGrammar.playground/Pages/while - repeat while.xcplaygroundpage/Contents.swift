//: [Previous](@previous)

/***************************while循环 / repeat-while循环 *************************/

import Foundation

//while循环
var n = 2
while n < 100 {
    n = n * 2
}
print(n)


//repeat-while循环 和 while循环的区别是在判断循环条件之前，先执行一次循环的代码块。然后重复循环直到条件为 false
var m = 2
repeat {
    m = m * 2
} while m < 100
print(m)

