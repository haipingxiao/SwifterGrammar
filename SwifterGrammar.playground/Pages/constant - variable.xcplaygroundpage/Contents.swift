//: [Previous](@previous)

import Foundation

/***************************基本数据类型***********************/
/**************************************************************
 1.整数: Int
 2.浮点数:Double表示64位浮点数，Float表示32位浮点数
 3.布尔类型: Bool,布尔值只有ture 和 flase 两种
 4.字符串: String
 5.字符:Character
 ***************************************************************/

/***************************变量和常量*************************/
/**************************************************************
 1.常量:值不能被修改，let修饰
 2.变量:只能被修改，var修饰
 *************************************************************/

var a = 20
a = 10
let b = 20
//b = 10  常量不能修改:error:'b' is a 'let' constant

/****************************************************************
 1.会自动推导声明的变量或常量的属性
 2.使用【option + 单击】建查看属性的类型
 ****************************************************************/

//自动推导类型
let str = "Swifter grammar"
let intValue = 10
let floatValue = 1.2

//指定数据类型
let doubleValue:Double = 10






//: [Next](@next)
