//: [Previous](@previous)



/***************************for循环*************************/

import Foundation

//变量i在[0,5）循环
for i in 0..<10 {
    print(i)
}

//变量i在[0,5]循环
for i in 0...5 {
    print(i)
}

// 使用 "_" 忽略不关心的值
for _ in 0..<5 {
    print("Swifter3.0 grammar")
}

print("----步长循环-----")
// 递增
for i in stride(from: 0, to: 12, by: 2) {
    print(i)
}

// 递减
for i in stride(from: 12, to: 0, by: -2) {
    print(i)
}

print("----反序循环----")
// 反序循环
for i in (0...10).reversed(){
    print(i)
}




