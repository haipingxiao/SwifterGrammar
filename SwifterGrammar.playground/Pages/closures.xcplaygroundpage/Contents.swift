//: [Previous](@previous)

import Foundation


/***************************最简单的闭包***********************/
//demo为没有参数，没有返回值的函数(可以省略参数、返回值、in)
let demo = {
    print("Hello, World!")
}
demo()

/***************************带参数的闭包************************/
/********* 1.格式:参数列表 -> 返回值类型in实现代码 *************/
/********* 2.参数、返回值、实现代码都是写在{}中    *************/
/********* 3.必须以"in"关键字分割定义和实现        *************/
/***************************************************************/
let demo1 = {
    (x: Int) -> () in
    print(x)
}
//有参数，无返回值
demo1(50)

//有参数，有返回值
let demo2 = {
    (x: Int) ->(Int) in
    return 40 + x
}
print(demo2(40))


//: [Next](@next)
